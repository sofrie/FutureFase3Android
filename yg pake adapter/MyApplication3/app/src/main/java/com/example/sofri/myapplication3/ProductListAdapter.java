package com.example.sofri.myapplication3;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by sofri on 8/13/2017.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>{

    String[] productList;
    Context context;
    IListComminicator listComminicator;

    public ProductListAdapter(Context context,String[] productList) {
        this.productList = productList;
        this.context=context;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //



        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_list,parent, false);

        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, final int position) {
        holder.itemTextView.setText(productList[position]);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listComminicator.onClick(productList[position]);
            }
        });

    }

    @Override
    public int getItemCount() {
        return productList.length ;
    }



    public class ProductViewHolder extends RecyclerView.ViewHolder
    {
        TextView itemTextView;
        CardView cardView;

        public ProductViewHolder(View itemView) {
            super(itemView);
            //itemTextView = (TextView) itemView.findViewById(R.id.tv_item);
            cardView=(CardView)itemView.findViewById(R.id.cv_product);
        }
    }

}
