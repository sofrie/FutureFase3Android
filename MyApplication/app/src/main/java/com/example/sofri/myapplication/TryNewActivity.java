package com.example.sofri.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TryNewActivity extends AppCompatActivity {

    TextView prevActivityTextView;
    Button okButton;
    Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_try_new);


        okButton=(Button) findViewById(R.id.btn_ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("tes","tes");
                Intent intent=new Intent();
                intent.putExtra("result","OK Button Pressed");
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        cancelButton=(Button) findViewById((R.id.btn_cancel));


        prevActivityTextView = (TextView) findViewById(R.id.tv_prev_activity) ;
        if(getIntent().hasExtra("previousClass")) {
            prevActivityTextView.setText(getIntent().getExtras().getString("previousClass"));
            Log.i("ANDROID_TAG", "change page");
        }
    }
    @Override
    protected void onStart( ) {
        super.onStart();
        Log.i("ANDROID_TAG", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("ANDROID_TAG", "onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("ANDROID_TAG", "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("ANDROID_TAG", "onRestart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("ANDROID_TAG","onPause");
    }

}
