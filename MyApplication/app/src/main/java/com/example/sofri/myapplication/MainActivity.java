package com.example.sofri.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button goToSecondActivity;
    public static final Integer REQUEST_CODE=101;
    TextView statusTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        statusTextView=(TextView) findViewById(R.id.tv_status);
        goToSecondActivity=(Button) findViewById(R.id.btn_go_to_second_activity);
        goToSecondActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,TryNewActivity.class);
                intent.putExtra("previousClass","TryNewActivity");
                startActivity(intent);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });


        //buat pindah halaman
        //Intent intent=new Intent (this,ActivityName.class)
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode, data);
        if(requestCode == REQUEST_CODE)
        {
            if(resultCode==RESULT_OK)
            {
                statusTextView=(TextView) findViewById(R.id.tv_status);
                if(data.hasExtra("result"))
                {
                    Log.i("tes result","tes result");
                    statusTextView.setText(data.getExtras().getString("result"));
                }
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.activity_main);
        Log.i("ANDROID_TAG","change page");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("ANDROID_TAG", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("ANDROID_TAG", "onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("ANDROID_TAG", "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("ANDROID_TAG", "onRestart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("ANDROID_TAG","onPause");
    }
}




